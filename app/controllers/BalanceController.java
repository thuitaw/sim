package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import exceptions.NonExistentAccountException;
import com.fasterxml.jackson.databind.node.ObjectNode;
import models.Account;
import play.libs.Json;
import play.db.jpa.JPA;
import play.db.jpa.Transactional;
import play.mvc.Controller;
import play.mvc.Result;

import javax.persistence.TypedQuery;
import javax.persistence.NoResultException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Root;
import java.util.List;

import static play.libs.Json.newObject;
import static play.libs.Json.toJson;

/**
 * Created by thuita on 22/10/2016.
 */

public class BalanceController extends Controller {

    @Transactional
    public Result get_balance(Integer account_number) {

        try {
            Account account = this.get_account(account_number);
            ObjectNode result = Json.newObject();
            result.put("account_balance", account.getBalance());
            return ok(result);

        } catch (NonExistentAccountException e) {
            e.printStackTrace();
            return notFound("Non existent account");
        }
    }


    private Account get_account(Integer account_number) throws NonExistentAccountException {
        TypedQuery<Account> query = (TypedQuery<Account>) JPA.em().createQuery(
                "SELECT acc FROM Account acc WHERE acc.account_number = :account_number", Account.class);
        query.setParameter("account_number", account_number);
        try{
            Account account = query.getSingleResult();
            return account;

        } catch (NoResultException e){
            String message = String.format("Account number %1$d not found",
                    account_number);
            throw new NonExistentAccountException(message);

        }


    }
}