package controllers;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import models.Account;
import models.Customer;
import play.db.jpa.JPA;
import play.db.jpa.Transactional;
import play.libs.Json;
import static play.libs.Json.toJson;
import play.mvc.*;
import utilities.AutoIncrementedInteger;


import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.io.IOException;


/**
 * Created by thuita on 21/10/2016.
 */
public class UserController extends Controller{


    @Transactional
    public Result add_customer(){
        ObjectMapper mapper = new ObjectMapper();
        try {
            JsonNode json = request().body().asJson();
            int account_number = this.generate_account_number();
            ((ObjectNode)json).put("account_number", account_number);
            Customer customer = mapper.readValue(json.toString(),
                    Customer.class);
            JPA.em().persist(customer);
            this.generate_account(account_number);

            ObjectNode result = Json.newObject();
            result.put("Customer", Json.toJson(customer));
            return created(result);

        } catch (JsonMappingException e) {
            e.printStackTrace();
            return badRequest("Could not map fields to customer object");
        } catch (JsonParseException e) {
            e.printStackTrace();
            return badRequest("Malformed Json");
        } catch (IOException e) {
            e.printStackTrace();
            return badRequest("Error occured, please try again later");
    }}


    @Transactional
    public Result view_all_customers(){

        CriteriaBuilder builder = JPA.em().getCriteriaBuilder();
        CriteriaQuery <Customer> criteriaQuery = builder.createQuery(Customer.class);
        Root<Customer> root = criteriaQuery.from(Customer.class);
        CriteriaQuery<Customer> all = criteriaQuery.select(root);
        TypedQuery <Customer> allQuery = JPA.em().createQuery(all);
        JsonNode jsonNodes = toJson(allQuery.getResultList());
        return ok(jsonNodes);

    }

    private synchronized Integer generate_account_number()
    {
        AutoIncrementedInteger a = new AutoIncrementedInteger();
        return a.incremented_integer;
    }


    public void generate_account(int account_number)  {
       ObjectMapper mapper = new ObjectMapper();
        JsonNode node = mapper.createObjectNode();
        ((ObjectNode)node).put("account_number", account_number);
        Account account = null;
        try {
            account = mapper.readValue(node.toString(),
                        Account.class);
        } catch (IOException e) {
            e.printStackTrace();
        }

        JPA.em().persist(account);
    }
}
