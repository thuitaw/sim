package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import exceptions.NonExistentAccountException;
import models.Account;
import models.DepositTransaction;
import play.cache.CacheApi;
import play.db.jpa.JPA;
import play.db.jpa.Transactional;
import play.mvc.Controller;
import play.mvc.Result;


import javax.persistence.TypedQuery;
import play.libs.Json;
import javax.inject.Inject;
import java.time.LocalDate;
import java.time.LocalDateTime;


/**
 * Created by thuita on 22/10/2016.
 */
public class DepositController extends Controller {

    private static final double MAX_DEPOSIT_PER_TRANSACTION = 40000;
    private static final double MAX_DEPOSIT_PER_DAY = 40000;
    private static final int MAX_TRANSACTIONS_PER_DAY= 4;

    private CacheApi cache;



    @Inject
    public DepositController(CacheApi cache){
        this.cache = cache;

    }

    @Transactional
    public Result deposit(){

        JsonNode json = request().body().asJson();

        if (json == null)
        {
            return badRequest("Only json data allowed for this method");
        }

        Double amount = json.findPath("amount").asDouble();
        Integer account_number = json.findPath("account_number").asInt();

        if (amount == null)
        {
            return badRequest("Missing Field [amount]");
        }

        if (account_number == null)
        {
            return badRequest("Missing Field [account_number]");
        }

        if (this.is_max_deposit_per_transaction(amount))
        {
            return forbidden("You cannot deposit more than 40000 in a single transaction");
        }

        try{
            Account account = this.get_account(account_number);
            if (this.has_reached_max_transaction_amount_per_day(account)) {
                return forbidden("You have reached the maximum amount allowed for deposit per account on a single day 150000");
            }

            if (this.has_reached_max_transactions_per_day(account)){
                return forbidden("You have reached the maximum number of allowed transactions per day");
            }
            this.deposit_amount(account.getAccount_id(), amount, account.getBalance());
            ObjectNode result = Json.newObject();
            result.put("status", "successfull");
            return created(result);



        } catch (NonExistentAccountException e) {
            e.printStackTrace();
            return notFound(e.getMessage());
        }
         catch (Exception e){
             return internalServerError();

         }

    }


    private Account get_account(Integer account_number) throws NonExistentAccountException {
        TypedQuery<Account> query = (TypedQuery<Account>) JPA.em().createQuery(
                "SELECT acc FROM Account acc WHERE acc.account_number = :account_number", Account.class);
        query.setParameter("account_number", account_number);
        Account account = query.getSingleResult();
        if (account != null){
            return account;
        }
        else{
            String message = String.format("Account number %1$d not found",
                    account_number);
            throw new NonExistentAccountException(message);
        }

    }
    private void deposit_amount(Long account_id, double amount, double current_amount) {
        Double new_amount = amount + current_amount;
        Account account = JPA.em().find(Account.class, account_id);
        account.setBalance(new_amount);
        JPA.em().persist(account);

    }


    private void generate_new_account_transaction(Account acc){
        DepositTransaction depositTransaction = new DepositTransaction();
        depositTransaction.setAmount(acc.getBalance());
        depositTransaction.setSet_time(this.get_current_date_as_string());
        depositTransaction.setTransactions_count(1); // initial amount
        cache.set(acc.getAccount_id().toString().concat("_deposit"), depositTransaction, 60 * 24 * 60); //cache for at least 24 hours
    }

    private boolean has_reached_max_transaction_amount_per_day(Account acc){
        boolean return_value;
        DepositTransaction depositTransaction = cache.get(acc.getAccount_id().toString().concat("_deposit"));

        if (depositTransaction != null){
            LocalDate set_date = this.parse_date_string(depositTransaction.getSet_time());
            if (depositTransaction.getAmount() >= MAX_DEPOSIT_PER_DAY && this.check_is_within_same_day(set_date) ) {
                return_value = true;
            }
            else{
                // update amount and time
                if (!this.check_is_within_same_day(set_date)){
                    depositTransaction.setSet_time(this.get_current_date_as_string());
                    depositTransaction.setAmount(0.0); // reset to 0

                }
                cache.set(acc.getAccount_id().toString().concat("_deposit"), depositTransaction);
                return_value = false;
            }
        }
        else{
            this.generate_new_account_transaction(acc);
            return_value = false;

        }

        return return_value;
    }


    private boolean has_reached_max_transactions_per_day(Account acc){
        boolean return_value;
        DepositTransaction depositTransaction = cache.get(acc.getAccount_id().toString().concat("_deposit"));

        if (depositTransaction != null){
            LocalDate set_date = this.parse_date_string(depositTransaction.getSet_time());
            if (depositTransaction.getTransactions_count() >= MAX_TRANSACTIONS_PER_DAY && this.check_is_within_same_day(set_date)){
                return_value = true;
            }
            else{
                // update the transaction count
                if (!this.check_is_within_same_day(set_date)){
                    depositTransaction.setSet_time(this.get_current_date_as_string());
                }
                depositTransaction.setTransactions_count(depositTransaction.getTransactions_count()+1);
                cache.set(acc.getAccount_id().toString().concat("_deposit"), depositTransaction);
                return_value = false;

            }
        }
        else{
            this.generate_new_account_transaction(acc);
            return_value = false;

        }

        return return_value;
    }



    private boolean is_max_deposit_per_transaction(double amount)

    {
        if (amount <= MAX_DEPOSIT_PER_TRANSACTION){
            return false;
        }
        return true;
    }

    private LocalDate get_current_date(){
        LocalDateTime current_date_time = LocalDateTime.now();
        LocalDate date = current_date_time.toLocalDate();
       return date;


    }

    private String get_current_date_as_string(){
        return this.get_current_date().toString();
    }
    private LocalDate parse_date_string(String date){
        LocalDate parsed_date = LocalDate.parse(date);
        return parsed_date;
    }

    private boolean check_is_within_same_day(LocalDate stored_date){
        if (stored_date.getDayOfYear() == this.get_current_date().getDayOfYear()){
            return true;
        }
        else{
            return false;
        }
    }
}
