package utilities;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by thuita on 22/10/2016.
 */
public class AutoIncrementedInteger {
    private static final AtomicInteger count = new AtomicInteger(10000000);
    public int incremented_integer;

    public AutoIncrementedInteger() {
        incremented_integer = count.incrementAndGet();

    }
}
