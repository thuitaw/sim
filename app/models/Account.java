package models;

/**
 * Created by thuita on 21/10/2016.
 */

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.Entity;

@Entity
public class Account {

    @Id
    @GeneratedValue
    @Column private Long account_id;
    @Column private Integer account_number;

    @Column(name = "balance", nullable = false, columnDefinition = "int default 0")
    private double balance;


    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public Integer getAccount_number() {
        return account_number;
    }

    public void setAccount_number(Integer account_number) {
        this.account_number = account_number;
    }

    public Long getAccount_id() {
        return account_id;
    }

    public void setAccount_id(Long account_id) {
        this.account_id = account_id;
    }



}
