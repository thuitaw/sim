package models;

/**
 * Created by thuita on 21/10/2016.
 */
import java.util.*;
import javax.persistence.*;

import play.db.*;



@Entity
public class Customer {
    @Id
    @GeneratedValue
    public Long id;

    @Column public String name;
    @Column public Integer age;
    @Column public String gender;

    @Column public Integer account_number;


    public Customer(String name, Integer age, String gender)
    {
        this.name = name;
        this.age = age;
        this.gender = gender;
    }


    public Customer()
    {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }


    public Integer getAccount_number() {
        return account_number;
    }

    public void setAccount_number(Integer account_number) {
        this.account_number = account_number;
    }
}
