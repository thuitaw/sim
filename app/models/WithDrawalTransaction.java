package models;

/**
 * Created by thuita on 22/10/2016.
 */
public class WithDrawalTransaction {

    /*
    Object used to track details of withdrawal transactions in memory
     */

    public Double amount;
    public String set_time;
    public Integer transactions_count;
    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getSet_time() {
        return set_time;
    }

    public void setSet_time(String set_time) {
        this.set_time = set_time;
    }

    public Integer getTransactions_count() {
        return transactions_count;
    }

    public void setTransactions_count(Integer transactions_count) {
        this.transactions_count = transactions_count;
    }




}
