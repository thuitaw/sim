package exceptions;

/**
 * Created by thuita on 22/10/2016.
 */
public class NonExistentAccountException extends Exception {
    public NonExistentAccountException(){

    }

    public NonExistentAccountException(String message){
        super(message);
    }
}
